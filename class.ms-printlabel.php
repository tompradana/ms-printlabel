<?php
require_once __DIR__ . '/vendor/autoload.php';
use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;
use chillerlan\QRCode\Output\QRImage;
use chillerlan\QRCode\Output\QRImageOptions;

class MS_Print_Label {
	private static $action_slug = 'print-label';

	public function __construct() {
		register_activation_hook( __FILE__, array( $this, 'ms_pl_activation' ) );
		$this->ms_pl_init();
	}

	public function ms_pl_activation() {
		// activation hook
	}

	public function ms_pl_init() {
		// init & hooked function
		add_action( 'woocommerce_admin_order_actions', array( $this, 'ms_pl_admin_order_actions' ), 10, 2 );
		add_action( 'wp_ajax_ms_print_label', array( $this, 'ms_pl_print_label_action' ) );
		add_action( 'admin_head', array( $this, 'ms_pl_admin_order_actions_icon' ) );
		add_action( 'admin_footer', array( $this, 'ms_pl_admin_scripts' ), 999 );
	}

	public function ms_pl_courier_list() {
		$list = array(
			'cod'	=> 'Cash on delivery',
			'gojek'	=> 'Go-Jek',
			'grab'	=> 'Grab',
			'jne' 	=> 'JNE',
			'tiki' 	=> 'TIKI',
			'pos' 	=> 'POS Indonesia',
			'sap' 	=> 'SAP Express',
			'jnt' 	=> 'J&T Express',
			'ninja'	=> 'Ninja Xpress'
		);
		return apply_filters( 'ms_pl_courier_list', $list );
	}

	/**
	 * Scripts
	 */
	public function ms_pl_admin_scripts() {
		global $current_screen, $hook;
		if ( $current_screen->id !== 'edit-shop_order' ) return;
		?>
		<div ms_pl_modal>
			<div ms_pl_modal_close>
				<a>&times;</a>
			</div>
			<div ms_pl_modal_body>
				<h3 ms_pl_modal_title></h3>
				<p>
					<b><label>Ekspedisi</label></b>
					<select ms_pl_label_shipping class="widefat">
						<option value="">Jasa pengiriman</option>
						<?php foreach( $this->ms_pl_courier_list() as $key => $kurir ) : ?>
							<option value="<?php echo $key; ?>"><?php echo $kurir; ?></option>
						<?php endforeach; ?>
					</select>
				</p>
				<p>
					<b><label>Layanan</label></b>
					<input type="text" ms_pl_label_service class="widefat" placeholder="Reguler">
				</p>
				<p>
					<b><label>Catatan</label></b>
					<textarea placeholder="Jangan di banting" ms_pl_label_notes class="widefat"></textarea>
				</p>
				<p><a ms_pl_print_label class="button button-primary button-large widefat"><?php _e( 'Download / Print Label', 'ms-printlabel' ); ?></a></p>
			</div>
		</div>
		<script type="text/javascript">
			function getParameterByName(name, url) {
				if (!url) url = window.location.href;
				name = name.replace(/[\[\]]/g, '\\$&');
				var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
				results = regex.exec(url);
				if (!results) return null;
				if (!results[2]) return '';
				return decodeURIComponent(results[2].replace(/\+/g, ' '));
			}

			(function($){
				$('a.print-label').on('click',function(e){
					e.preventDefault();
					$('[ms_pl_label_notes],[ms_pl_label_service],[ms_pl_label_shipping]').val('');
					var thisUrl = $(this).attr('href');
					var order_id = getParameterByName('order_id', thisUrl);
					$('body').addClass('ms_pl_modal');
					$('div[ms_pl_modal]').addClass('open');
					$('div[ms_pl_modal_body] [ms_pl_modal_title]').html('Print label pesanan #' + order_id);
					$('div[ms_pl_modal_body] [ms_pl_print_label]').attr('href', thisUrl);
					console.log(e);
				});
				$('[ms_pl_modal_close] a').on('click',function(){
					$('body').removeClass('ms_pl_modal');
					$('div[ms_pl_modal]').removeClass('open');
				});
				$('a[ms_pl_print_label]').on('click',function(e){
					e.preventDefault();
					var thisUrl = $(this).attr('href');
					var notes = $('[ms_pl_label_notes]').val();
					var kurir = $('[ms_pl_label_shipping]').val();
					var service = $('[ms_pl_label_service]').val();
					if ( '' != notes ) {
						thisUrl += '&notes='+encodeURIComponent(notes); 
					}
					if ( '' != kurir ) {
						thisUrl += '&courier='+kurir; 
						if ( '' != service ) {
							thisUrl += '&service='+encodeURIComponent(service); 
						}
					} else {
						alert( 'Anda belum memilih kurir' );
						return false;
					}
					window.location.href = thisUrl;
				});
			})(jQuery);
		</script>
		<?php
	}

	/**
	 * WooCommerce funtion, hook and filter
	 */
	public function ms_pl_admin_order_actions( $actions, $order ) {
		$action_slug 	= $this::$action_slug;
		$actions[$action_slug] = array(
			'url' 		=> wp_nonce_url( admin_url( 'admin-ajax.php?action=ms_print_label&order_id=' . $order->get_id() ),
				'ms-print-label' ),
			'name'      => __( 'Print Label', 'ms-printlabel' ),
			'action'    => $action_slug,
		);
		return $actions;
	}

	public function ms_pl_print_label_action() {
		$order_id = isset( $_REQUEST['order_id'] ) ? $_REQUEST['order_id'] : false;
		$label_notes = isset( $_REQUEST['notes'] ) ? $_REQUEST['notes'] : false;
		$kurir = isset( $_REQUEST['courier'] ) ? $_REQUEST['courier'] : false;
		$service = isset( $_REQUEST['service'] ) ? $_REQUEST['service'] : 'Reguler';
		if ( in_array( $kurir, array( 'gojek', 'grab' ) ) ) {
			$service = '';
		};
		if ( !$order_id ) exit;
		$order 					= wc_get_order( $order_id );
		$items 					= $order->get_items();
		$total_products			= count( $items );
		$total_items 			= $order->get_item_count();
		$shipping_m     		= $order->get_shipping_method();
		$payment_m 				= $order->get_payment_method();
		$total 		   			= $order->get_order_item_totals()['order_total']['value'];
		$url_order      		= $order->get_view_order_url();
		$mpdf 					= new \Mpdf\Mpdf();
		$qroptions 				= new QROptions([
			'scale'	=> 3,
			'addQuietzone' => false,
		]);
		$qrcode 				= new QRCode($qroptions);
		$qr_image 			= $qrcode->render($url_order);
		ob_start();
		echo '<div style="width: 600px; box-sizing: border-box; padding: 0; font-family: helvetica, serif; font-size: 13px; line-height: 1.4; margin: 0 auto;">
		<div style="width: 100%; border: 1px solid #000; box-sizing: border-box">
		<div style="box-sizing: border-box; padding: 5px 20px 5px 20px; border-bottom: 1px solid #aaa">
		<table style="font-size: 13px; width: 100%; text-align: left; font-family: helvetica, serif;">
		<tr>
		<td><img src="' . plugins_url( 'assets/images/logo-2.png', __FILE__ ) . '" style="width: auto; height: 75px;"></td>';
		if ( $kurir && 'cod' !== $kurir ) { 
			$kurir_n = $this->ms_pl_courier_list()[$kurir];
			echo '<td style="vertical-align:middle;text-align:right"></td>';
			echo '<td style="vertical-align:middle;">';
			echo '<div style="float:right;text-align:left;">';
			echo '<table style="font-size: 13px; width: 100%; text-align: left; font-family: helvetica, serif;">';
			echo '<tr>';
			echo '<td style="text-align:right;"><img style="margin-right:10px;" src="' . plugins_url( 'assets/logoekspedisi/'.$kurir.'.png', __FILE__ ) . '"></td>';
			printf( '<td><div style="float:right;text-align:left;">%s<br/><b>%s</b></div></td>', $kurir_n, $service );
			echo '</tr>';
			echo '</table>';
			echo '<div style="clear:both"></div>';
			echo '</div>';
			echo '</td>';
		};
		echo '</tr>
		</table>
		</div>
		<div style="box-sizing: border-box; padding: 15px 20px 0 20px">					
		<div style="width: 15%; float: left">
		<div>
		'.sprintf( '%s<br/><b>%s</b>', __( 'Berat:' ), $this->ms_pl_get_total_weight( $items ) ).'
		</div>
		</div>
		<div style="width: 25%; float: left">
		<div>
		'.sprintf( '%s<br/><b>%s</b>', __( 'Ongkir:' ), $order->get_total_shipping() ) .'
		</div>
		</div>
		<div style="width: 48.5%; float: right">
		<div style="float: left; width: 200px">
		'.sprintf( '%s<br/><b>#%s</b><br/><span style="font-family:monospace; color: #999; font-size: 10px">%s</span>', __( 'No. Order:' ), $order_id, $order->get_order_key() ).'
		</div>
		<div style="float: right; width: 50px">
		<img src="' . $qr_image . '" style="width: auto; height: 50px;">
		</div>
		<div style="clear:both"></div>
		</div>
		<div style="clear:both"></div>
		</div>';
		if ( $label_notes ) :
			echo '<div style="box-sizing: border-box; padding: 15px 20px 0 20px">
			<div style="border: 1px solid #999; border-radius: 4px; padding: 8px; text-align: center; font-weight: bold; font-style: italic">' . $label_notes . '</div>
			</div>';
		endif;
		echo '<div style="box-sizing: border-box; padding: 15px 20px 15px 20px; border-bottom: 1px dashed #aaa">
		<div style="margin: 0 0 0">
		<div style="width: 48.5%; float:left">' . $this->ms_pl_get_billing_address( $order ) .'</div>
		<div style="width: 48.5%; float:right">' . $this->ms_pl_get_shop_address() .'</div>
		<div style="clear:both"></div>
		</div>
		</div>
		<div style="box-sizing: border-box; padding: 15px 20px 15px 20px">
		<table width="100" border="0" cellpadding="0" cellspacing="0" style="font-size: 13px; width: 100%; text-align: left; font-family: helvetica, serif;">
		<thead style="color: #777; font-weight: normal; text-align: left;">
		<tr>
		<th style="font-size: 13px;color: #777;padding-bottom: 5px;font-weight: normal; text-align: left; width: 65%;">Produk</th>
		<th style="font-size: 13px;color: #777;padding-bottom: 5px;font-weight: normal; text-align: left; width: 100px">SKU</th>
		<th style="font-size: 13px;color: #777;padding-bottom: 5px;font-weight: normal; text-align: right; width: 80x">Jumlah</th>
		</tr>
		</thead>
		<tbody style="font-family: helvetica, serif; line-height: 20px; color: #111">';
		$i = 1;
		foreach( $items as $product_id => $product_item ) :
			$i_data 	= $product_item->get_data();
			$product 	= $product_item->get_product();
			$name 		= $product->get_name();
			$sku 			= $product->get_sku();
			$qty 			= $product_item->get_quantity();
			$color  	= $i % 2 == 0 ? '#e3e3e3' : '#f3f3f3';
			printf( '<tr style="font-family: helvetica, serif; background-color: %5$s">
				<td style="font-size: 13px;vertical-align: top; padding: 5px 20px 5px 5px; text-align: left">
				%2$s
				</td>
				<td style="font-size: 13px;vertical-align: top; padding: 5px 5px 5px 0">
				%3$s
				</td>
				<td style="font-size: 13px;vertical-align: top; text-align: right; padding: 5px 5px 5px 5px">
				%4$s pcs
				</td>
				</tr>' , $i, $name, $sku, $qty, $color );
			$i++; endforeach;
			if ( $payment_m == 'cod' ) {
				echo '</tbody>
				<tfoot>
				<tr>
				<td colspan="2" style="border-top: 1px solid #ccc; padding: 10px 0 0 0">
				<b>Total:</b>
				</td>
				<td style="border-top: 1px solid #ccc; padding: 10px 0 0 0;text-align:right">
				<b style="float: right">' . $total . '</b>
				</td>
				</tr>
				</tfoot>';
			};
			echo '</tbody>
			</table>
			</div>
			</div>
			</div>';
			$html = ob_get_contents();
			ob_end_clean();
			$filename = 'order_' .$order_id . '_' . $kurir . '_label.pdf';
			$mpdf->WriteHTML($html);
			$mpdf->Output($filename,'D');
			exit;
		}

		public function ms_pl_get_billing_address( $order = null ) {
			if ( null === $order ) {
				return;
			}
			$order_id = $order->get_id();

			// billing
			$billing_address 	= $order->get_formatted_billing_address();
			$name 						= $order->get_formatted_billing_full_name();
			$address 					= $order->get_address( 'billing' );
			$phone 						= $order->get_billing_phone();
			$city 						= '' <> get_post_meta( $order_id, '_billing_city', true ) ? get_post_meta( $order_id, '_billing_city', true ) : $address['city'];

			// shipping
			$shipping_address = $order->get_formatted_shipping_address();
			if ( '' <> $shipping_address ) {
				$name 					= $order->get_formatted_shipping_full_name();
				$address 				= $order->get_address( 'shipping' );
				$city 					= '' <> get_post_meta( $order_id, '_shipping_city', true ) ? get_post_meta( $order_id, '_shipping_city', true ) : $address['city'];
			}

			$kecamatan = get_post_meta( $order_id, '_billing_district', true );

			$formated_addr    = array();
			$formated_addr[] 	= __( 'Kepada:', 'ms-printlabel' );
			$formated_addr[] 	= '<strong>' . $name . '</strong>';
			$formated_addr[] 	= $address['address_1'];
			if ( isset( $address['address_2'] ) && '' <> $address['address_2'] ) {
				$formated_addr[] = $address['address_2'];
			}
			$formated_addr[]  = $city . ( '' <> $kecamatan ? ', ' . $kecamatan : '' );
			$formated_addr[]  = WC()->countries->get_states( $address['country'] )[$address['state']] . ( isset( $address['postcode'] ) && '' != $address['postcode'] ? ' ' . $address['postcode'] : '' );
			$formated_addr[]  = WC()->countries->countries[$address['country']];
			if ( '' <> $phone ) {
				$formated_addr[] = $phone;
			}

			return implode( '<br/>', $formated_addr );
		}

		// public function ms_pl_get_billing_address( $order = null ) {
		// 	if ( null === $order ) {
		// 		return;
		// 	}
		// 	$billing_address = $order->get_formatted_billing_address();
		// 	$name = $order->get_formatted_billing_full_name();
		// 	$phone = $order->get_billing_phone();

		// 	$shipping_address = $order->get_formatted_shipping_address();
		// 	if ( '' <> $shipping_address ) {
		// 		$billing_address = $shipping_address;
		// 		$name = $order->get_formatted_shipping_full_name();
		// 	}
		// 	return sprintf(
		// 		'%s<br/>
		// 		%s
		// 		%s', 
		// 		__( 'Kepada:', 'ms-printlabel' ), 
		// 		str_replace(trim($name), '<b>'.$name.'</b>', $billing_address),
		// 		( '' <> $phone ? '<div>' . $phone . '</div>' : '' )
		// 	);
		// }

		public function ms_pl_get_shop_address( $order = null ) {
			$custom_address = get_option( 'ms_settings_pl_from_address' );
			$address1 = WC()->countries->get_base_address();
			$address2 = WC()->countries->get_base_address_2();
			$city 		= WC()->countries->get_base_city();
			$postcode = WC()->countries->get_base_postcode();
			$state 		= WC()->countries->get_states(WC()->countries->get_base_country())[WC()->countries->get_base_state()];
			$country 	= WC()->countries->countries[WC()->countries->get_base_country()];
			if ( $custom_address == '' || false === $custom_address ) {
				return sprintf( 
					'%1$s<br/>
					<strong>%2$s</strong><br/>
					%3$s<br/>
					%4$s
					%5$s, %6$s %7$s<br/>
					%8$s<br/>',
					__( 'Dari:', 'ms-printlabel' ),
					get_bloginfo( 'name' ),
					$address1,
					( '' <> $address2 ? $address2 . '</br>' : '' ),
					$city, $state, $postcode,
					$country
				);
			} else {
				return str_replace( PHP_EOL, '<br/>', $custom_address );
			}
		}

		public function ms_pl_admin_order_actions_icon() {
			$action_slug 	= $this::$action_slug;
			echo '<style>.wc-action-button-'.$action_slug.'::after { font-family: woocommerce !important; content: "\e02b" !important; }</style>';
			echo '<style type="text/css" media="screen">
			div[ms_pl_modal] {
				visibility: hidden;
				opacity: 0;
				transition: .25s ease all;
				position: fixed;
				z-index: 999999;
				width: 400px;
				background-color: #fff;
				top: 50%;
				left: 50%;
				box-shadow: 0 10px 30px rgba(0,0,0,.3);
				border-radius: 8px;
				transform: translate( -50%,-50% );
			}
			div[ms_pl_modal] .button {
				text-align: center;
			}
			div[ms_pl_modal].open {
				visibility: visible;
				opacity: 1;
			}
			div[ms_pl_modal_close] {
				text-align: right;
			}
			div[ms_pl_modal_close] a {
				display: inline-block;
				color: red;
				margin: 10px 10px 0 0;
				font-size: 20px;
				cursor: pointer;
			}
			div[ms_pl_modal_body] {
				padding: 0 30px 20px;
			}
			div[ms_pl_modal_body] textarea {
				min-height: 100px;
			}
			body.ms_pl_modal {
				position: relative;
				height: 100%;
				overflow: hidden;
			}
			body.ms_pl_modal:before {
				content: "";
				display: block;
				position: fixed;
				z-index: 991999;
				top: 0;
				right: 0;
				left: 0;
				bottom: 0;
				background-color: rgba(0,0,0,.7)
			}
			</style>';
		}

		public function ms_pl_get_total_weight( $items ) {
			$total_weight = 0;
			$weight_unit  = get_option( 'woocommerce_weight_unit' );
			foreach( $items as $item_id => $product_item ) {
				$quantity = $product_item->get_quantity();
				$product = $product_item->get_product(); 
				$product_weight = $product->get_weight();
				$total_weight += floatval( (int) $product_weight * $quantity );
			}
			return $this->ms_convert_to_kilogram( $total_weight, $weight_unit );
			return $total_weight;
		}

	/**
	 * Helper function
	 */
	public function ms_convert_to_kilogram( $weight = 0, $unit = 'g' ) {
		$unit_ = ' Kg';
		switch ( $unit ) {
			case 'g':
			return round( $weight/1000, 1 ) . $unit_;
			break;
			case 'lbs':
			return round( $weight/2.20462, 1 ) . $unit_;
			break;
			case 'oz':
			return round( $weight/35.274, 1 ) . $unit_;
			break;
			default: 
			return round( $weight, 1 ) . $unit_;
			break;
		}
	}

	public function ms_log( $log ) {
		if ( true === WP_DEBUG ) {
			if (is_array($log) || is_object($log)) {
				error_log(print_r($log, true));
			} else {
				error_log($log);
			}
		}
	}
}
global $MS_Print_Label;
$MS_Print_Label = new MS_Print_Label();