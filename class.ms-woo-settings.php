<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'WC_MS_Plugin_Settings' ) ) :

/**
 * Settings class
 *
 * @since 1.0.0
 */
class WC_MS_Plugin_Settings extends WC_Settings_Page {
	public function __construct() {
		$this->id    = 'mswoosettings';
		$this->label = __( 'MS Woo', 'ms-printlabel' );

		add_filter( 'woocommerce_settings_tabs_array',        array( $this, 'add_settings_page' ), 20 );
		add_action( 'woocommerce_settings_' . $this->id,      array( $this, 'output' ) );
		add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'save' ) );

		// only add this if you need to add sections for your settings tab
		add_action( 'woocommerce_sections_' . $this->id,      array( $this, 'output_sections' ) );
	}

	public function get_sections() {
		$sections = array(
			''  => __( 'General', 'ms-printlabel' )
    );
    return apply_filters( 'woocommerce_get_sections_' . $this->id, $sections );
	}

	public function output() {
    global $current_section;
    $settings = $this->get_settings( $current_section );
    WC_Admin_Settings::output_fields( $settings );
	}

	public function save() {
    global $current_section;
    $settings = $this->get_settings( $current_section );
    WC_Admin_Settings::save_fields( $settings );
	}

	public function get_settings( $current_section = '' ) {
		$settings = '';
		return apply_filters( 'woocommerce_get_settings_' . $this->id, $settings, $current_section );
	}
}

endif;

return new WC_MS_Plugin_Settings();