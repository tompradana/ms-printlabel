<?php
$settings_id = 'mswoosettings';

add_filter( 'woocommerce_get_sections_'.$settings_id, 'ms_pl_settings_section' );
function ms_pl_settings_section( $sections ) {
	$sections['wc_pl_section'] = __( 'Print Label', 'ms-printlabel' );
	return $sections;
}

add_filter( 'woocommerce_get_settings_'.$settings_id, 'ms_woo_general_settings', 10, 2 );
function ms_woo_general_settings( $settings, $current_section ) {
	if ( $current_section == '' ) {
		$settings = array();
		$settings[] = array( 
			'name' => __( 'MS Woo General Settings', 'ms-printlabel' ), 
			'type' => 'title', 
			'desc' => __( 'The following options are used to configure MS Woo Addon', 'ms-printlabel' ), 
			'id' => '' 
		);
	}
	return $settings;
}

/**
 * Custom settings
 */
add_filter( 'woocommerce_get_settings_'.$settings_id, 'ms_pl_settings', 10, 2 );
function ms_pl_settings( $settings, $current_section ) {
	global $MS_Print_Label;
	$key = 'ms_settings_';
	if ( $current_section == 'wc_pl_section' ) {
		$settings = array();
		$settings[] = array( 
			'name' => __( 'MS Print Label Settings', 'ms-printlabel' ), 
			'type' => 'title', 
			'id' => '' 
		);
		$settings[] = array(
			'name'     => __( 'Informasi Pengirim', 'text-domain' ),
			'desc_tip' => __( 'Jika kosong, maka akan menggunakan data dari pengaturan shop base', 'text-domain' ),
			'id'       => $key . 'pl_from_address',
			'type'     => 'textarea',
			'desc'     => __( 'Informasi pengirim seperti nama toko alamat & nomor telpon.', 'text-domain' ),
		);
		$settings[] = array( 'type' => 'sectionend', 'id' => '' );
	}
	return $settings;
}
