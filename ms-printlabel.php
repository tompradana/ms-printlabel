<?php
/**
 * Plugin Name: MS Print Label
 * Version: 1.0.0
 * Description: Print shipping label, for WooCommerce | Support: tom.wpdev@gmail.com | Phone: 08113644664 
 */

if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

/**
 * Check WooCommerce
 */
if ( ! function_exists( 'is_woocommerce_activated' ) ) {
	function is_woocommerce_activated() {
		if ( function_exists('WC') ) { return true; } else { return false; }
	}
}

/**
 * Notice
 */
function ms_pl_activation_notice() {
	if ( !is_woocommerce_activated() ) {
		$action 	= 'install-plugin';
		$slug 		= 'woocommerce';
		$url 		= wp_nonce_url( add_query_arg( array(
			'action' => $action,
			'plugin' => $slug
		), admin_url( 'update.php') ), $action.'_'.$slug );
		$class 		= 'notice notice-error';
		$message 	= __( 'Plugin MS-PrintLabel memerlukan plugin WooCommerce. <a href="%s">Silahkan install dan aktifkan plugin WooCommerce</a>.', 'ms-printlabel' );
		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), sprintf(  $message, $url ) ); 
	} 
}
add_action( 'admin_notices', 'ms_pl_activation_notice' );

/**
 * Lets go
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	include( 'class.ms-printlabel.php' );

	/** initiate settings **/
	function ms_woo_add_settings( $settings ) {
		$settings[] = include( 'class.ms-woo-settings.php' );
		return $settings;
	}
	add_filter( 'woocommerce_get_settings_pages', 'ms_woo_add_settings' );

	/** custom settings **/
	include 'functions/settings.php';
}




